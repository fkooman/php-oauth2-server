<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateTimeImmutable;
use fkooman\OAuth\Server\Exception\InvalidGrantException;
use RangeException;

class AuthorizationCode
{
    public const TOKEN_TYPE = 'authorization_code';

    private string $tokenId;
    private string $authKey;
    private UserInfo $userInfo;
    private string $clientId;
    private Scope $scope;
    private DateTimeImmutable $expiresAt;
    private string $redirectUri;
    private ?string $codeChallenge;
    private ?string $nonce;

    public function __construct(string $tokenId, string $authKey, UserInfo $userInfo, string $clientId, Scope $scope, DateTimeImmutable $expiresAt, string $redirectUri, ?string $codeChallenge, ?string $nonce)
    {
        $this->tokenId = $tokenId;
        $this->authKey = $authKey;
        $this->userInfo = $userInfo;
        $this->clientId = $clientId;
        $this->scope = $scope;
        $this->expiresAt = $expiresAt;
        $this->redirectUri = $redirectUri;
        $this->codeChallenge = $codeChallenge;
        $this->nonce = $nonce;
    }

    public static function fromJson(string $jsonString): self
    {
        $jsonData = Json::decode($jsonString);

        try {
            if (self::TOKEN_TYPE !== Extractor::requireString($jsonData, 'type')) {
                throw new InvalidGrantException(\sprintf('"type" MUST be "%s"', self::TOKEN_TYPE));
            }

            return new self(
                Extractor::requireString($jsonData, 'token_id'),
                Extractor::requireString($jsonData, 'auth_key'),
                UserInfo::fromData(Extractor::requireArray($jsonData, 'user_info')),
                Extractor::requireString($jsonData, 'client_id'),
                new Scope(Extractor::requireString($jsonData, 'scope')),
                new DateTimeImmutable(Extractor::requireString($jsonData, 'expires_at')),
                Extractor::requireString($jsonData, 'redirect_uri'),
                Extractor::optionalString($jsonData, 'code_challenge'),
                Extractor::optionalString($jsonData, 'nonce')
            );
        } catch (RangeException $e) {
            throw new InvalidGrantException($e->getMessage());
        }
    }

    public function tokenId(): string
    {
        return $this->tokenId;
    }

    public function authKey(): string
    {
        return $this->authKey;
    }

    public function userInfo(): UserInfo
    {
        return $this->userInfo;
    }

    public function clientId(): string
    {
        return $this->clientId;
    }

    public function scope(): Scope
    {
        return $this->scope;
    }

    public function expiresAt(): DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function redirectUri(): string
    {
        return $this->redirectUri;
    }

    public function codeChallenge(): ?string
    {
        return $this->codeChallenge;
    }

    public function nonce(): ?string
    {
        return $this->nonce;
    }

    public function json(): string
    {
        return Json::encode(
            array_merge(
                [
                    'type' => self::TOKEN_TYPE,
                    'token_id' => $this->tokenId,
                    'auth_key' => $this->authKey,
                    'user_info' => $this->userInfo->toData(),
                    'client_id' => $this->clientId,
                    'scope' => (string) $this->scope,
                    'expires_at' => $this->expiresAt->format(DateTimeImmutable::ATOM),
                    'redirect_uri' => $this->redirectUri,
                ],
                null !== $this->codeChallenge ? ['code_challenge' => $this->codeChallenge] : [],
                null !== $this->nonce ? ['nonce' => $this->nonce] : []
            )
        );
    }
}
