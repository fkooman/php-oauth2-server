<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateTimeImmutable;
use fkooman\OAuth\Server\Exception\InvalidTokenException;
use fkooman\OAuth\Server\Exception\TokenEncoderException;
use fkooman\OAuth\Server\Http\Request;

class BearerValidator implements ValidatorInterface
{
    protected DateTimeImmutable $dateTime;
    private TokenEncoderInterface $tokenEncoder;
    private AccessTokenVerifierInterface $accessTokenVerifier;

    public function __construct(TokenEncoderInterface $tokenEncoder, AccessTokenVerifierInterface $accessTokenVerifier)
    {
        $this->tokenEncoder = $tokenEncoder;
        $this->accessTokenVerifier = $accessTokenVerifier;
        $this->dateTime = Dt::get();
    }

    public function validate(?Request $request = null): AccessToken
    {
        $request ??= Request::fromServerVariables();

        try {
            // check signature
            $accessToken = AccessToken::fromJson(
                $this->tokenEncoder->decode($request->bearerToken()),
                $request->bearerToken() // rawToken
            );

            // check access_token expiry
            if ($this->dateTime >= $accessToken->expiresAt()) {
                throw new InvalidTokenException('"access_token" expired');
            }

            // Perform additional verification of the AccessToken
            $this->accessTokenVerifier->verify($accessToken);

            return $accessToken;
        } catch (TokenEncoderException $e) {
            throw new InvalidTokenException(\sprintf('invalid "access_token": %s', $e->getMessage()));
        }
    }
}
