<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DomainException;
use fkooman\OAuth\Server\Exception\InvalidClientException;

class ClientInfo
{
    private string $clientId;

    /** @var array<string> */
    private array $redirectUris;

    private ?string $clientSecret;

    private ?string $clientName;

    private bool $requireApproval;

    private bool $requirePkce;

    private bool $requireNonce;

    private Scope $allowedScope;

    private ?string $sectorIdentifierUri;

    /**
     * @param array<string> $redirectUris
     */
    private function __construct(string $clientId, array $redirectUris, ?string $clientSecret, ?string $clientName, bool $requireApproval, bool $requirePkce, bool $requireNonce, Scope $allowedScope, ?string $sectorIdentifierUri)
    {
        $this->clientId = $clientId;
        if (0 === \count($redirectUris)) {
            throw new DomainException('must have >=1 "redirect_uri"');
        }
        $this->redirectUris = $redirectUris;
        $this->clientSecret = $clientSecret;
        $this->clientName = $clientName;
        $this->requireApproval = $requireApproval;
        $this->requirePkce = $requirePkce;
        $this->requireNonce = $requireNonce;
        $this->allowedScope = $allowedScope;
        $this->sectorIdentifierUri = $sectorIdentifierUri;
    }

    public function clientId(): string
    {
        return $this->clientId;
    }

    public function clientSecret(): ?string
    {
        return $this->clientSecret;
    }

    /**
     * Get the scope object with scopes this OAuth client is able to request.
     */
    public function allowedScope(): Scope
    {
        return $this->allowedScope;
    }

    /**
     * Get the name of the client, Client ID if not set.
     */
    public function clientName(): string
    {
        return $this->clientName ?? $this->clientId;
    }

    public function requireApproval(): bool
    {
        return $this->requireApproval;
    }

    public function requirePkce(): bool
    {
        return $this->requirePkce;
    }

    /**
     * Whether or not the RP MUST use the `nonce` query parameter.
     */
    public function requireNonce(): bool
    {
        return $this->requireNonce;
    }

    /**
     * @return array<string>
     */
    public function redirectUris(): array
    {
        return $this->redirectUris;
    }

    public function isValidRedirectUri(string $redirectUri): bool
    {
        if (\in_array($redirectUri, $this->redirectUris, true)) {
            // exact string match, make sure the URL does not contain the
            // literal "{PORT}" we use for "native clients"
            // @see https://todo.sr.ht/~fkooman/php-oauth2-server/3
            if (false !== strpos($redirectUri, '{PORT}')) {
                return false;
            }

            return true;
        }

        // parsing is NOT great... but don't see how to avoid it here, we need
        // to accept all ports and both IPv4 and IPv6 for loopback entries
        foreach ($this->redirectUris as $clientRedirectUri) {
            // IPv4 loopback
            if (0 === strpos($clientRedirectUri, 'http://127.0.0.1:{PORT}/')) {
                if (self::portMatch($clientRedirectUri, $redirectUri)) {
                    return true;
                }
            }

            // IPv6 loopback
            if (0 === strpos($clientRedirectUri, 'http://[::1]:{PORT}/')) {
                if (self::portMatch($clientRedirectUri, $redirectUri)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * For calculation of OpenID "Pairwise Identifier".
     *
     * @see https://openid.net/specs/openid-connect-core-1_0.html#PairwiseAlg
     */
    public function determineSectorIdentifier(): string
    {
        if (null !== $this->sectorIdentifierUri) {
            return self::hostFromUri($this->sectorIdentifierUri);
        }

        if (1 === \count($this->redirectUris)) {
            return self::hostFromUri($this->redirectUris[0]);
        }

        throw new InvalidClientException('unable to determine "sector_identifier"');
    }

    public function sectorIdentifierUri(): ?string
    {
        return $this->sectorIdentifierUri;
    }

    public function save(): string
    {
        return Json::encode(
            array_merge(
                [
                    'client_id' => $this->clientId,
                    'redirect_uris' => $this->redirectUris,
                    'scope' => (string) $this->allowedScope,
                    'require_approval' => $this->requireApproval,
                    'require_pkce' => $this->requirePkce,
                    'require_nonce' => $this->requireNonce,
                ],
                null !== $this->clientSecret ? ['client_secret' => $this->clientSecret] : [],
                null !== $this->clientName ? ['client_name' => $this->clientName] : [],
                null !== $this->sectorIdentifierUri ? ['sector_identifier_uri' => $this->sectorIdentifierUri] : []
            )
        );
    }

    public static function load(string $clientInfo): self
    {
        return self::fromData(Json::decode($clientInfo));
    }

    public static function fromData(array $clientData): self
    {
        // We take the field names from "OAuth 2.0 Dynamic Client Registration
        // Protocol" as much as possible.
        // @see https://www.rfc-editor.org/rfc/rfc7591
        return new self(
            Extractor::requireString($clientData, 'client_id'),
            Extractor::requireStringArray($clientData, 'redirect_uris'),
            Extractor::optionalString($clientData, 'client_secret'),
            Extractor::optionalString($clientData, 'client_name'),
            Extractor::optionalBool($clientData, 'require_approval') ?? true,
            Extractor::optionalBool($clientData, 'require_pkce') ?? true,
            Extractor::optionalBool($clientData, 'require_nonce') ?? true,
            new Scope(Extractor::requireString($clientData, 'scope')),
            Extractor::optionalString($clientData, 'sector_identifier_uri')
        );
    }

    private static function hostFromUri(string $inUri): string
    {
        $urlHost = parse_url($inUri, \PHP_URL_HOST);
        if (!\is_string($urlHost) || 0 === \strlen($urlHost)) {
            throw new DomainException();
        }

        return $urlHost;
    }

    private static function portMatch(string $clientRedirectUri, string $redirectUri): bool
    {
        $uriPort = parse_url($redirectUri, \PHP_URL_PORT);
        if (!\is_int($uriPort)) {
            return false;
        }

        // parse_url already makes sure that the port is between 0 and 65535,
        // i.e.: port >= 0 && port <= 65535, we make it a little more strict to
        // require >= 1024 which makes sure (on at least Linux) that user
        // processes can claim that port
        if ($uriPort < 1024) {
            return false;
        }
        $clientRedirectUriWithPort = str_replace('{PORT}', (string) $uriPort, $clientRedirectUri);

        return $redirectUri === $clientRedirectUriWithPort;
    }
}
