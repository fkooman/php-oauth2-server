<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateInterval;
use DateTimeImmutable;

class UserInfo
{
    private string $userId;
    private DateTimeImmutable $authorizationExpiresAt;

    private ?DateTimeImmutable $authTime;

    private ?string $acrValue;

    /** @var ?array<string> */
    private ?array $amrValues;

    /**
     * @param ?array<string> $amrValues
     */
    public function __construct(string $userId, ?DateTimeImmutable $authorizationExpiresAt = null, ?DateTimeImmutable $authTime = null, ?string $acrValue = null, ?array $amrValues = null)
    {
        $this->userId = $userId;
        $this->authorizationExpiresAt = $authorizationExpiresAt ?? date_create_immutable()->add(new DateInterval('P90D'));
        $this->acrValue = $acrValue;
        $this->amrValues = $amrValues;
        $this->authTime = $authTime;
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function authorizationExpiresAt(): DateTimeImmutable
    {
        return $this->authorizationExpiresAt;
    }

    public function acrValue(): ?string
    {
        return $this->acrValue;
    }

    /**
     * @return ?array<string>
     */
    public function amrValues(): ?array
    {
        return $this->amrValues;
    }

    public function authTime(): ?DateTimeImmutable
    {
        return $this->authTime;
    }

    public function toData(): array
    {
        return array_merge(
            [
                'user_id' => $this->userId,
                'authorization_expires_at' => $this->authorizationExpiresAt->format(DateTimeImmutable::ATOM),
            ],
            null !== $this->authTime ? ['auth_time' => $this->authTime->format(DateTimeImmutable::ATOM)] : [],
            null !== $this->acrValue ? ['acr_value' => $this->acrValue] : [],
            null !== $this->amrValues ? ['amr_values' => $this->amrValues] : [],
        );
    }

    public static function fromData(array $userInfoData): self
    {
        if (null !== $authTime = Extractor::optionalString($userInfoData, 'auth_time')) {
            $authTime = new DateTimeImmutable($authTime);
        }

        return new self(
            Extractor::requireString($userInfoData, 'user_id'),
            new DateTimeImmutable(Extractor::requireString($userInfoData, 'authorization_expires_at')),
            $authTime,
            Extractor::optionalString($userInfoData, 'acr_value'),
            Extractor::optionalStringArray($userInfoData, 'amr_values'),
        );
    }
}
