<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Http;

use fkooman\OAuth\Server\Exception\InvalidTokenException;
use fkooman\OAuth\Server\Extractor;
use RangeException;

class Request
{
    private array $serverData;
    private array $getData;
    private array $postData;

    public function __construct(array $serverData, array $getData, array $postData)
    {
        $this->serverData = $serverData;
        $this->getData = $getData;
        $this->postData = $postData;
    }

    public static function fromServerVariables(): self
    {
        return new self($_SERVER, $_GET, $_POST);
    }

    public function query(): Query
    {
        return new Query($this->getData);
    }

    public function post(): Post
    {
        return new Post($this->postData);
    }

    public function requestMethod(): string
    {
        return Extractor::requireString($this->serverData, 'REQUEST_METHOD');
    }

    public function pathInfo(): string
    {
        if (null !== $pathInfo = Extractor::optionalString($this->serverData, 'PATH_INFO')) {
            return $pathInfo;
        }

        return '/';
    }

    /**
     * Determine whether the caller is a browser or a simple (JSON) HTTP
     * client.
     */
    public function isBrowser(): bool
    {
        if (null === $httpAccept = Extractor::optionalString($this->serverData, 'HTTP_ACCEPT')) {
            return false;
        }

        return false !== stripos($httpAccept, 'text/html');
    }

    public function bearerToken(): string
    {
        try {
            $authorizationHeader = Extractor::requireString($this->serverData, 'HTTP_AUTHORIZATION');
            if (0 !== strpos($authorizationHeader, 'Bearer ')) {
                throw new InvalidTokenException();
            }

            // b64token    = 1*( ALPHA / DIGIT /
            //                   "-" / "." / "_" / "~" / "+" / "/" ) *"="
            // credentials = "Bearer" 1*SP b64token
            if (1 !== preg_match('|^Bearer [a-zA-Z0-9-._~+/]+=*$|', $authorizationHeader)) {
                throw new InvalidTokenException('invalid token');
            }

            return substr($authorizationHeader, 7);
        } catch (RangeException $e) {
            throw new InvalidTokenException();
        }
    }
}
