<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Http;

use DateInterval;
use fkooman\OAuth\Server\Exception\InvalidRequestException;
use fkooman\OAuth\Server\Scope;

class Query
{
    private array $getData;

    public function __construct(array $getData)
    {
        $this->getData = $getData;
    }

    public function responseType(): string
    {
        $responseType = $this->requireParameter('response_type');
        if (!\in_array($responseType, ['code'], true)) {
            throw new InvalidRequestException('invalid "response_type"');
        }

        return $responseType;
    }

    public function clientId(): string
    {
        $clientId = $this->requireParameter('client_id');

        // client-id  = *VSCHAR
        // VSCHAR     = %x20-7E
        if (1 !== preg_match('/^[\x20-\x7E]+$/', $clientId)) {
            throw new InvalidRequestException('invalid "client_id"');
        }

        return $clientId;
    }

    public function codeChallenge(): ?string
    {
        if (null === $codeChallenge = $this->optionalParameter('code_challenge')) {
            return null;
        }

        // it seems the length of the codeChallenge is always 43 because it is
        // the output of the SHA256 hashing algorithm
        if (1 !== preg_match('/^[\x41-\x5A\x61-\x7A\x30-\x39-_]{43}$/', $codeChallenge)) {
            throw new InvalidRequestException('invalid "code_challenge"');
        }

        return $codeChallenge;
    }

    public function redirectUri(): string
    {
        // NOTE: no need to validate the redirect_uri, as we do strict string
        // comparison
        return $this->requireParameter('redirect_uri');
    }

    public function scope(): Scope
    {
        return new Scope($this->requireParameter('scope'));
    }

    public function state(): string
    {
        $state = $this->requireParameter('state');

        // state      = 1*VSCHAR
        // VSCHAR     = %x20-7E
        if (1 !== preg_match('/^[\x20-\x7E]+$/', $state)) {
            throw new InvalidRequestException('invalid "state"');
        }

        return $state;
    }

    public function nonce(): ?string
    {
        if (null === $nonce = $this->optionalParameter('nonce')) {
            return null;
        }

        // NOTE: there is no official syntax for "nonce", so (for now) we use
        // the one of the "state" variable...
        //
        // state      = 1*VSCHAR
        // VSCHAR     = %x20-7E
        if (1 !== preg_match('/^[\x20-\x7E]+$/', $nonce)) {
            throw new InvalidRequestException('invalid "nonce"');
        }

        return $nonce;
    }

    public function maxAge(): ?DateInterval
    {
        if (null === $maxAge = $this->optionalParameter('max_age')) {
            return null;
        }

        if (1 !== preg_match('/^0$|^[1-9][0-9]*$/', $maxAge)) {
            throw new InvalidRequestException('invalid "max_age"');
        }

        return new DateInterval(\sprintf('PT%dS', $maxAge));
    }

    public function postLogoutRedirectUri(): string
    {
        return $this->requireParameter('post_logout_redirect_uri');
    }

    public function idTokenHint(): string
    {
        return $this->requireParameter('id_token_hint');
    }

    /**
     * @return ?array<string>
     */
    public function acrValues(): ?array
    {
        if (null === $acrValues = $this->optionalParameter('acr_values')) {
            return null;
        }

        return explode(' ', $acrValues);
    }

    private function requireParameter(string $parameterName): string
    {
        if (!\array_key_exists($parameterName, $this->getData)) {
            throw new InvalidRequestException(\sprintf('missing "%s" parameter', $parameterName));
        }

        if (!\is_string($this->getData[$parameterName])) {
            throw new InvalidRequestException(\sprintf('"%s" parameter MUST be string', $parameterName));
        }

        return $this->getData[$parameterName];
    }

    private function optionalParameter(string $parameterName): ?string
    {
        if (!\array_key_exists($parameterName, $this->getData)) {
            return null;
        }

        if (!\is_string($this->getData[$parameterName])) {
            throw new InvalidRequestException(\sprintf('"%s" parameter MUST be string', $parameterName));
        }

        return $this->getData[$parameterName];
    }
}
