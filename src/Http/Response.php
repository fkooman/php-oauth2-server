<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Http;

class Response
{
    private int $statusCode;

    /** @var array<string,string> */
    private array $responseHeaders;

    private string $responseBody;

    /**
     * @param array<string,string> $responseHeaders
     */
    public function __construct(string $responseBody, array $responseHeaders = [], int $statusCode = 200)
    {
        $this->responseBody = $responseBody;
        $this->responseHeaders = $responseHeaders;
        $this->statusCode = $statusCode;
    }

    public function getBody(): string
    {
        return $this->responseBody;
    }

    /**
     * @return array<string,string>
     */
    public function getHeaders(): array
    {
        return $this->responseHeaders;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function send(): void
    {
        http_response_code($this->statusCode);
        foreach ($this->responseHeaders as $k => $v) {
            header(\sprintf('%s: %s', $k, $v));
        }
        echo $this->responseBody;
    }
}
