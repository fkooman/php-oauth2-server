<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use fkooman\OAuth\Server\Exception\TokenEncoderException;
use RangeException;

/**
 * @see https://github.com/ietf-jose/cookbook/blob/master/jwe/5_6.direct_encryption_using_aes-gcm.json
 * @see https://datatracker.ietf.org/doc/html/draft-amringer-jose-chacha-02
 */
class SimpleEncryptedTokenEncoder implements TokenEncoderInterface
{
    private string $secretKey;
    private string $keyId;

    public function __construct(string $encodedKeyData)
    {
        try {
            $keyData = Json::decode($encodedKeyData);
            if ('XC20P' !== Extractor::requireString($keyData, 'alg')) {
                throw new TokenEncoderException('invalid key `alg`');
            }
            if ('oct' !== Extractor::requireString($keyData, 'kty')) {
                throw new TokenEncoderException('invalid key `kty`');
            }
            if ('enc' !== Extractor::requireString($keyData, 'use')) {
                throw new TokenEncoderException('invalid key `use`');
            }
            $secretKey = Base64UrlSafe::decode(Extractor::requireString($keyData, 'k'));
            if (\SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_KEYBYTES !== \strlen($secretKey)) {
                throw new TokenEncoderException('invalid key `k`');
            }
            $this->secretKey = $secretKey;
            $this->keyId = Extractor::requireString($keyData, 'kid');
        } catch (RangeException $e) {
            throw new TokenEncoderException(\sprintf('invalid key: %s', $e->getMessage()));
        }
    }

    public static function generateKey(): string
    {
        return \sprintf(
            '{"alg":"XC20P","k":"%s","kid":"%s","kty":"oct","use":"enc"}',
            Base64UrlSafe::encodeUnpadded(sodium_crypto_aead_xchacha20poly1305_ietf_keygen()),
            Base64UrlSafe::encodeUnpadded(random_bytes(16))
        );
    }

    public function encode(string $tokenPayload): string
    {
        return $this->encodeWithNonce(
            $tokenPayload,
            random_bytes(\SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES)
        );
    }

    public function decode(string $encodedToken): string
    {
        $splitEncodedToken = explode('.', $encodedToken, 5);
        if (5 !== \count($splitEncodedToken)) {
            throw new TokenEncoderException('invalid token format');
        }
        [$encodedHeader,, $encodedNonceBytes, $encodedCipherText, $encodedTagBytes] = $splitEncodedToken;
        $expectedEncodedHeader = $this->tokenHeader();
        if (!hash_equals($expectedEncodedHeader, $encodedHeader)) {
            throw new TokenEncoderException('invalid token header');
        }

        $plainText = sodium_crypto_aead_xchacha20poly1305_ietf_decrypt(
            Base64UrlSafe::decode($encodedCipherText) . Base64UrlSafe::decode($encodedTagBytes),
            $expectedEncodedHeader,
            Base64UrlSafe::decode($encodedNonceBytes),
            $this->secretKey
        );

        if (false === $plainText) {
            throw new TokenEncoderException('unable to decrypt token');
        }

        return $plainText;
    }

    protected function encodeWithNonce(string $tokenPayload, string $nonceBytes): string
    {
        $tokenHeader = $this->tokenHeader();
        $cipherTextWithTag = sodium_crypto_aead_xchacha20poly1305_ietf_encrypt(
            $tokenPayload,
            $tokenHeader,
            $nonceBytes,
            $this->secretKey
        );

        return \sprintf(
            '%s..%s.%s.%s',
            $tokenHeader,
            Base64UrlSafe::encodeUnpadded($nonceBytes),
            Base64UrlSafe::encodeUnpadded(substr($cipherTextWithTag, 0, -\SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES)),
            Base64UrlSafe::encodeUnpadded(substr($cipherTextWithTag, -\SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES))
        );
    }

    private function tokenHeader(): string
    {
        // we construct the header from string, without doing a JSON encode,
        // because JSON encode is not necessarily "stable", i.e. I don't think
        // it is required to always return 100% the same output as it depends
        // on the specific JSON implementation...
        return Base64UrlSafe::encodeUnpadded(
            \sprintf(
                '{"alg":"dir","enc":"XC20P","kid":"%s"}',
                $this->keyId
            )
        );
    }
}
