<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateInterval;
use fkooman\OAuth\Server\Exception\InvalidClientException;
use fkooman\OAuth\Server\Exception\InvalidRequestException;
use fkooman\OAuth\Server\Http\RedirectResponse;
use fkooman\OAuth\Server\Http\Request;
use fkooman\OAuth\Server\Http\Response;
use ValueError;

abstract class OpenIDServer extends OAuthServer
{
    private const SALT_LENGTH = 32;

    private OpenIDConfig $config;
    private string $saltValue;

    public function __construct(OpenIDConfig $config, StorageInterface $storage, ClientDbInterface $clientDb, TokenEncoderInterface $tokenEncoder, string $saltValue)
    {
        parent::__construct(
            $storage,
            $clientDb,
            $tokenEncoder,
            $config->issuerIdentity
        );
        $this->config = $config;
        if (self::SALT_LENGTH !== \strlen($saltValue)) {
            throw new ValueError(\sprintf('"salt" MUST have length %d', self::SALT_LENGTH));
        }
        $this->saltValue = $saltValue;
    }

    /**
     * Validates the authorization request from the client and returns verified
     * data to show an authorization dialog.
     *
     * @return array{client_id:string,client_name:string,scope:string,redirect_uri:string,max_age:?\DateInterval,acr_values:?array<string>}
     */
    public function getOpenIDAuthorize(?Request $request = null): array
    {
        $request ??= Request::fromServerVariables();

        return array_merge(
            $this->getAuthorize($request),
            [
                'max_age' => $request->query()->maxAge(),
                'acr_values' => $request->query()->acrValues(),
            ]
        );
    }

    public function openIDMetadata(
        string $authorizationEndpoint,
        string $tokenEndpoint,
        ?string $userInfoEndpoint = null,
        ?string $jwksUri = null,
        ?string $endSessionEndpoint = null
    ): array {
        return array_merge(
            $this->metadata($authorizationEndpoint, $tokenEndpoint),
            [
                'scopes_supported' => $this->config->scopesSupported->scopeTokenList(),
            ],
            null !== $this->config->idTokenSigningAlgValuesSupported ? ['id_token_signing_alg_values_supported' => $this->config->idTokenSigningAlgValuesSupported] : [],
            null !== $this->config->organizationName ? ['organization_name' => $this->config->organizationName] : [],
            null !== $this->config->acrValuesSupported ? ['acr_values_supported' => $this->config->acrValuesSupported] : [],
            null !== $userInfoEndpoint ? ['userinfo_endpoint' => $userInfoEndpoint] : [],
            null !== $jwksUri ? ['jwks_uri' => $jwksUri] : [],
            null !== $endSessionEndpoint ? ['end_session_endpoint' => $endSessionEndpoint] : [],
        );
    }

    public function endSession(string $userId, ?Request $request = null): Response
    {
        $request ??= Request::fromServerVariables();

        $idTokenHint = $request->query()->idTokenHint();
        $postLogoutRedirectUri = $request->query()->postLogoutRedirectUri();

        // we MUST ignore that the `id_token` is most likely expired...
        $idToken = $this->decodeIDToken($idTokenHint);

        $tokenIssuer = Extractor::requireString($idToken, 'iss');
        if ($tokenIssuer !== $this->config->issuerIdentity) {
            throw new InvalidRequestException('invalid "iss" in "id_token"');
        }
        $tokenAudience = Extractor::requireString($idToken, 'aud');

        if (null === $clientInfo = $this->clientDb->get($tokenAudience)) {
            throw new InvalidClientException(\sprintf('client "%s" does not exist', $tokenAudience));
        }

        // XXX i guess the next block is not super necessary, but well
        $tokenSub = Extractor::requireString($idToken, 'sub');
        $pairwiseId = $this->pairwiseId($clientInfo, $userId);
        if ($tokenSub !== $pairwiseId) {
            throw new InvalidRequestException(\sprintf('"sub" in "id_token" does not match expected value [expected=%s]', $pairwiseId));
        }

        foreach ($this->storage->getAuthorizations($userId) as $clientAuthorization) {
            if ($clientAuthorization->clientId() === $tokenAudience) {
                $this->storage->deleteAuthorization($clientAuthorization->authKey());
            }
        }

        $postLogoutRedirectUriHost = parse_url($postLogoutRedirectUri, \PHP_URL_HOST);
        if (!\is_string($postLogoutRedirectUriHost)) {
            throw new InvalidRequestException('unable to extract host from "post_logout_redirect_uri"');
        }

        $redirectUriHosts = [];
        foreach ($clientInfo->redirectUris() as $redirectUri) {
            $uriHost = parse_url($redirectUri, \PHP_URL_HOST);
            if (\is_string($uriHost)) {
                $redirectUriHosts[] = $uriHost;
            }
        }

        if (!\in_array($postLogoutRedirectUriHost, $redirectUriHosts, true)) {
            throw new InvalidRequestException('provided "post_logout_redirect_uri" is not allowed');
        }

        return new RedirectResponse($postLogoutRedirectUri);
    }

    public function pairwiseId(ClientInfo $clientInfo, string $userId): string
    {
        // Calculate sub = SHA-256 ( sector_identifier || local_account_id || salt ).
        return Base64UrlSafe::encodeUnpadded(
            hash(
                'sha256',
                $clientInfo->determineSectorIdentifier() . $userId . $this->saltValue,
                true
            )
        );
    }

    protected function generateIDToken(ClientInfo $clientInfo, AuthorizationCode $authorizationCode): ?string
    {
        $tokenData = [
            'iss' => $this->config->issuerIdentity,
            'sub' => $this->pairwiseId($clientInfo, $authorizationCode->userInfo()->userId()),
            'aud' => $authorizationCode->clientId(),
            'iat' => $this->dateTime->getTimestamp(),
            'nbf' => $this->dateTime->sub(new DateInterval('PT5S'))->getTimestamp(),
            // "NOTE: The ID Token expiration time is unrelated the
            // lifetime of the authenticated session between the RP and
            // the OP."
            // We chose to use the same value as the validity of the
            // "authorization_code".
            'exp' => $this->dateTime->add($this->authorizationCodeExpiry)->getTimestamp(),
        ];
        if (null !== $authTime = $authorizationCode->userInfo()->authTime()) {
            // TODO when e.g. `max_age` is requested by RP, we MUST return the
            // `auth_time` field!
            $tokenData['auth_time'] = $authTime->getTimestamp();
        }
        if (null !== $nonce = $authorizationCode->nonce()) {
            $tokenData['nonce'] = $nonce;
        }
        if (null !== $acrValue = $authorizationCode->userInfo()->acrValue()) {
            $tokenData['acr'] = $acrValue;
        }
        if (null !== $amrValues = $authorizationCode->userInfo()->amrValues()) {
            $tokenData['amr'] = $amrValues;
        }

        return $this->encodeIDToken($clientInfo, $tokenData);
    }

    abstract protected function encodeIDToken(ClientInfo $clientInfo, array $tokenData): string;

    /**
     * Decode an ID Token.
     *
     * NOTE: the token MAY be expired, we use it for "id_token_hint" validation
     * only.
     *
     * @see https://openid.net/specs/openid-connect-rpinitiated-1_0.html
     */
    abstract protected function decodeIDToken(string $encodedToken): array;
}
