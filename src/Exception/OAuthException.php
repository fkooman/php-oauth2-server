<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Exception;

use Exception;
use fkooman\OAuth\Server\Http\JsonResponse;

class OAuthException extends Exception
{
    private ?string $description;

    private int $statusCode;

    /** @var array<string,string> */
    private array $responseHeaders;

    /**
     * @param array<string,string> $responseHeaders
     */
    public function __construct(string $message, ?string $description, array $responseHeaders, int $statusCode)
    {
        parent::__construct($message);
        $this->statusCode = $statusCode;
        $this->description = $description;
        $this->responseHeaders = $responseHeaders;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getJsonResponse(): JsonResponse
    {
        $jsonData = [
            'error' => $this->message,
        ];
        if (null !== $this->description) {
            $jsonData['error_description'] = $this->description;
        }

        return new JsonResponse(
            $jsonData,
            $this->responseHeaders,
            $this->statusCode
        );
    }
}
