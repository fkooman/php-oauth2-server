<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateTimeImmutable;
use fkooman\OAuth\Server\Exception\InvalidTokenException;
use RangeException;

class AccessToken
{
    public const TOKEN_TYPE = 'access_token';

    private string $tokenId;
    private string $authKey;
    private UserInfo $userInfo;
    private string $clientId;
    private Scope $scope;
    private DateTimeImmutable $expiresAt;
    private ?string $rawToken;

    public function __construct(string $tokenId, string $authKey, UserInfo $userInfo, string $clientId, Scope $scope, DateTimeImmutable $expiresAt, ?string $rawToken = null)
    {
        $this->tokenId = $tokenId;
        $this->authKey = $authKey;
        $this->userInfo = $userInfo;
        $this->clientId = $clientId;
        $this->scope = $scope;
        $this->expiresAt = $expiresAt;
        $this->rawToken = $rawToken;
    }

    public static function fromJson(string $jsonString, string $rawToken): self
    {
        $jsonData = Json::decode($jsonString);

        try {
            if (self::TOKEN_TYPE !== Extractor::requireString($jsonData, 'type')) {
                throw new InvalidTokenException(\sprintf('"type" MUST be "%s"', self::TOKEN_TYPE));
            }

            return new self(
                Extractor::requireString($jsonData, 'token_id'),
                Extractor::requireString($jsonData, 'auth_key'),
                UserInfo::fromData(Extractor::requireArray($jsonData, 'user_info')),
                Extractor::requireString($jsonData, 'client_id'),
                new Scope(Extractor::requireString($jsonData, 'scope')),
                new DateTimeImmutable(Extractor::requireString($jsonData, 'expires_at')),
                $rawToken
            );
        } catch (RangeException $e) {
            throw new InvalidTokenException($e->getMessage());
        }
    }

    public function tokenId(): string
    {
        return $this->tokenId;
    }

    public function authKey(): string
    {
        return $this->authKey;
    }

    public function userInfo(): UserInfo
    {
        return $this->userInfo;
    }

    public function clientId(): string
    {
        return $this->clientId;
    }

    public function scope(): Scope
    {
        return $this->scope;
    }

    public function expiresAt(): DateTimeImmutable
    {
        return $this->expiresAt;
    }

    /**
     * Exposes the "raw" Bearer token. This is useful if you want to inspect
     * the token directly, e.g. extract the "Key ID". The token retrieved here
     * has been verified already!
     */
    public function raw(): ?string
    {
        return $this->rawToken;
    }

    public function json(): string
    {
        return Json::encode(
            [
                'type' => self::TOKEN_TYPE,
                'token_id' => $this->tokenId,
                'auth_key' => $this->authKey,
                'user_info' => $this->userInfo->toData(),
                'client_id' => $this->clientId,
                'scope' => (string) $this->scope,
                'expires_at' => $this->expiresAt->format(DateTimeImmutable::ATOM),
            ]
        );
    }
}
