<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateTimeImmutable;

interface StorageInterface
{
    public function getAuthorization(string $authKey): ?Authorization;

    public function storeAuthorization(string $userId, string $clientId, Scope $scope, string $authKey, DateTimeImmutable $authorizedAt, DateTimeImmutable $expiresAt): void;

    /**
     * @return array<Authorization>
     */
    public function getAuthorizations(string $userId): array;

    public function deleteAuthorization(string $authKey): void;

    public function isRefreshTokenReplay(string $refreshTokenId): bool;

    public function logRefreshToken(string $authKey, string $refreshTokenId): void;

    public function updateAuthorization(string $authKey, DateTimeImmutable $lastUsed): void;

    public function deleteExpiredAuthorizations(string $userId, DateTimeImmutable $dateTime): void;
}
