<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use DateInterval;
use DateTimeImmutable;
use fkooman\OAuth\Server\Exception\InvalidClientException;
use fkooman\OAuth\Server\Exception\InvalidGrantException;
use fkooman\OAuth\Server\Exception\InvalidRequestException;
use fkooman\OAuth\Server\Exception\InvalidScopeException;
use fkooman\OAuth\Server\Exception\TokenEncoderException;
use fkooman\OAuth\Server\Http\JsonResponse;
use fkooman\OAuth\Server\Http\RedirectResponse;
use fkooman\OAuth\Server\Http\Request;
use RuntimeException;

class OAuthServer
{
    protected const RANDOM_LENGTH = 32;

    protected DateTimeImmutable $dateTime;

    protected DateInterval $authorizationCodeExpiry;

    protected DateInterval $accessTokenExpiry;

    protected StorageInterface $storage;

    protected ClientDbInterface $clientDb;

    /**
     * RFC 9207 Issuer Identity.
     */
    protected string $issuerIdentity;

    private TokenEncoderInterface $tokenEncoder;

    public function __construct(StorageInterface $storage, ClientDbInterface $clientDb, TokenEncoderInterface $tokenEncoder, string $issuerIdentity)
    {
        // @see https://www.php.net/manual/en/mbstring.configuration.php#ini.mbstring.func-overload
        if (false !== (bool) \ini_get('mbstring.func_overload')) {
            throw new RuntimeException('"mbstring.func_overload" MUST NOT be enabled');
        }
        $this->storage = $storage;
        $this->clientDb = $clientDb;
        $this->tokenEncoder = $tokenEncoder;
        $this->issuerIdentity = $issuerIdentity;
        $this->dateTime = Dt::get();
        $this->authorizationCodeExpiry = new DateInterval('PT3M'); // 3 minutes
        $this->accessTokenExpiry = new DateInterval('PT1H');       // 1 hour
    }

    /**
     * Validates the authorization request from the client and returns verified
     * data to show an authorization dialog.
     *
     * @return array{client_id:string,client_name:string,scope:string,redirect_uri:string}
     */
    public function getAuthorize(?Request $request = null): array
    {
        $request ??= Request::fromServerVariables();
        $clientInfo = $this->validateAuthorizeRequest($request);

        return [
            'client_id' => $request->query()->clientId(),
            'client_name' => $clientInfo->clientName(),
            'scope' => (string) $request->query()->scope(),
            'redirect_uri' => $request->query()->redirectUri(),
        ];
    }

    /**
     * Validates the authorization request from the client and returns
     * authorize response in case the client does NOT require approval by the
     * user (resource owner).
     *
     * @return false|Http\Response
     */
    public function getAuthorizeResponse(UserInfo $userInfo, ?Request $request = null)
    {
        $request ??= Request::fromServerVariables();
        $clientInfo = $this->validateAuthorizeRequest($request);
        if ($clientInfo->requireApproval()) {
            return false;
        }

        return $this->postAuthorizeApproved($userInfo, $request);
    }

    public function postAuthorize(UserInfo $userInfo, ?Request $request = null): RedirectResponse
    {
        $request ??= Request::fromServerVariables();
        $this->validateAuthorizeRequest($request);
        if ('yes' === $request->post()->approve()) {
            return $this->postAuthorizeApproved($userInfo, $request);
        }

        // user did not approve, tell OAuth client
        return new RedirectResponse(
            $this->prepareRedirectUri(
                $request->query()->redirectUri(),
                [
                    'error' => 'access_denied',
                    'state' => $request->query()->state(),
                    'iss' => $this->issuerIdentity,
                ]
            )
        );
    }

    /**
     * Handles POST request to the "/token" endpoint of the OAuth server.
     */
    public function postToken(?Request $request = null): JsonResponse
    {
        $request ??= Request::fromServerVariables();
        $clientInfo = $this->identifyAndAuthenticateClient($request->post()->clientId(), $request->post()->clientSecret());
        switch ($request->post()->grantType()) {
            case 'authorization_code':
                return $this->postTokenAuthorizationCode($clientInfo, $request);

            case 'refresh_token':
                return $this->postTokenRefreshToken($clientInfo, $request);

            default:
                throw new InvalidRequestException('invalid "grant_type"');
        }
    }

    public function metadata(string $authorizationEndpoint, string $tokenEndpoint): array
    {
        return [
            'issuer' => $this->issuerIdentity,
            'authorization_endpoint' => $authorizationEndpoint,
            'token_endpoint' => $tokenEndpoint,
            'response_types_supported' => ['code'],
            'grant_types_supported' => ['authorization_code', 'refresh_token'],
            'token_endpoint_auth_methods_supported' => ['client_secret_post'],
            'code_challenge_methods_supported' => ['S256'],
        ];
    }

    protected function randomBytes(): string
    {
        return random_bytes(self::RANDOM_LENGTH);
    }

    protected function generateIDToken(ClientInfo $clientInfo, AuthorizationCode $authorizationCode): ?string
    {
        return null;
    }

    /**
     * Handles POST request to the "/authorize" endpoint of the OAuth server.
     *
     * This is typically the "form submit" on the "authorize dialog" shown in
     * the browser that the user then accepts or rejects.
     */
    private function postAuthorizeApproved(UserInfo $userInfo, Request $request): RedirectResponse
    {
        // every "authorization" has a unique key that is bound to the
        // authorization code, access tokens(s) and refresh token
        $authKey = Base64UrlSafe::encodeUnpadded($this->randomBytes());

        $authorizationCode = new AuthorizationCode(
            Base64UrlSafe::encodeUnpadded($this->randomBytes()),
            $authKey,
            $userInfo,
            $request->query()->clientId(),
            $request->query()->scope(),
            $this->dateTime->add($this->authorizationCodeExpiry),
            $request->query()->redirectUri(),
            $request->query()->codeChallenge(),
            $request->query()->nonce()
        );

        return new RedirectResponse(
            $this->prepareRedirectUri(
                $request->query()->redirectUri(),
                [
                    'code' => $this->tokenEncoder->encode($authorizationCode->json()),
                    'state' => $request->query()->state(),
                    'iss' => $this->issuerIdentity,
                ]
            )
        );
    }

    /**
     * Validate the request to the "/authorize" endpoint.
     */
    private function validateAuthorizeRequest(Request $request): ClientInfo
    {
        $clientInfo = $this->getClient($request->query()->clientId());

        // make sure the response_type is "code", the only one we support
        if ('code' !== $request->query()->responseType()) {
            throw new InvalidRequestException('invalid "response_type"');
        }

        // make sure the requested redirect URI is allowed for this client
        if (!$clientInfo->isValidRedirectUri($request->query()->redirectUri())) {
            throw new InvalidRequestException(
                \sprintf(
                    '"redirect_uri" [%s] not allowed for client "%s"',
                    $request->query()->redirectUri(),
                    $clientInfo->clientId()
                )
            );
        }

        // enforce PKCE if required
        if ($clientInfo->requirePkce() && null === $request->query()->codeChallenge()) {
            throw new InvalidRequestException(
                \sprintf(
                    '"code_challenge" parameter (PKCE) required for client "%s"',
                    $clientInfo->clientId()
                )
            );
        }

        // make sure the requested scope(s) are allowed for this client
        if (false === $clientInfo->allowedScope()->containsAll($request->query()->scope())) {
            throw new InvalidScopeException(
                \sprintf(
                    '"scope" [%s] not allowed for client "%s"',
                    (string) $request->query()->scope(),
                    $clientInfo->clientId()
                )
            );
        }

        // enforce use of "nonce" parameter (OpenID Connect) if required
        if ($request->query()->scope()->hasOne('openid')) {
            if ($clientInfo->requireNonce() && null === $request->query()->nonce()) {
                throw new InvalidRequestException(
                    \sprintf(
                        '"nonce" parameter required for client "%s"',
                        $clientInfo->clientId()
                    )
                );
            }
        }

        return $clientInfo;
    }

    private function postTokenAuthorizationCode(ClientInfo $clientInfo, Request $request): JsonResponse
    {
        try {
            // verify the authorization code signature
            $authorizationCode = AuthorizationCode::fromJson($this->tokenEncoder->decode($request->post()->code()));

            // check authorization_code expiry
            if ($this->dateTime >= $authorizationCode->expiresAt()) {
                throw new InvalidGrantException('"authorization_code" expired');
            }

            // make sure the code is bound to the client
            if ($clientInfo->clientId() !== $authorizationCode->clientId()) {
                throw new InvalidRequestException('unexpected "client_id"');
            }

            if ($request->post()->redirectUri() !== $authorizationCode->redirectUri()) {
                throw new InvalidRequestException('unexpected "redirect_uri"');
            }

            // verify PKCE if a code challenge was provided in the
            // authorization request (the OAuthServer::validateAuthorizeRequest
            // method makes sure PKCE was used if required)
            if (null !== $codeChallenge = $authorizationCode->codeChallenge()) {
                if (!self::verifyPkce($request->post()->codeVerifier(), $codeChallenge)) {
                    throw new InvalidGrantException('invalid "code_verifier"');
                }
            }

            // check whether authorization_code was already used
            if (null !== $this->storage->getAuthorization($authorizationCode->authKey())) {
                $this->storage->deleteAuthorization($authorizationCode->authKey());

                throw new InvalidGrantException('"authorization_code" was used before');
            }

            // delete user's expired authorizations
            $this->storage->deleteExpiredAuthorizations($authorizationCode->userInfo()->userId(), $this->dateTime);

            // store the authorization
            $this->storage->storeAuthorization(
                $authorizationCode->userInfo()->userId(),
                $authorizationCode->clientId(),
                $authorizationCode->scope(),
                $authorizationCode->authKey(),
                $this->dateTime,
                $authorizationCode->userInfo()->authorizationExpiresAt()
            );

            // make sure an access_token never outlives its authorization, i.e.
            // the refresh_token expiry
            $accessTokenExpiresAt = $this->dateTime->add($this->accessTokenExpiry);
            if ($accessTokenExpiresAt > $authorizationCode->userInfo()->authorizationExpiresAt()) {
                $accessTokenExpiresAt = $authorizationCode->userInfo()->authorizationExpiresAt();
            }

            $accessToken = new AccessToken(
                Base64UrlSafe::encodeUnpadded($this->randomBytes()),
                $authorizationCode->authKey(),
                $authorizationCode->userInfo(),
                $authorizationCode->clientId(),
                $authorizationCode->scope(),
                $accessTokenExpiresAt
            );

            $refreshToken = new RefreshToken(
                Base64UrlSafe::encodeUnpadded($this->randomBytes()),
                $authorizationCode->authKey(),
                $authorizationCode->userInfo(),
                $authorizationCode->clientId(),
                $authorizationCode->scope(),
                $authorizationCode->userInfo()->authorizationExpiresAt()
            );

            $jsonData = [
                'access_token' => $this->tokenEncoder->encode($accessToken->json()),
                'refresh_token' => $this->tokenEncoder->encode($refreshToken->json()),
                'token_type' => 'bearer',
                'expires_in' => $this->expiresAtToInt($accessTokenExpiresAt),
            ];

            // OpenID Connect
            if ($authorizationCode->scope()->hasOne('openid')) {
                if (null !== $idToken = $this->generateIDToken($clientInfo, $authorizationCode)) {
                    $jsonData['id_token'] = $idToken;
                }
            }

            return new JsonResponse(
                $jsonData,
                // The authorization server MUST include the HTTP "Cache-Control"
                // response header field [RFC2616] with a value of "no-store" in any
                // response containing tokens, credentials, or other sensitive
                // information, as well as the "Pragma" response header field [RFC2616]
                // with a value of "no-cache".
                [
                    'Cache-Control' => 'no-store',
                    'Pragma' => 'no-cache',
                ]
            );
        } catch (TokenEncoderException $e) {
            throw new InvalidGrantException(\sprintf('invalid "authorization_code": %s', $e->getMessage()));
        }
    }

    private function postTokenRefreshToken(ClientInfo $clientInfo, Request $request): JsonResponse
    {
        try {
            // verify the refresh code signature
            $refreshToken = RefreshToken::fromJson($this->tokenEncoder->decode($request->post()->refreshToken()));

            // check refresh_token expiry
            if ($this->dateTime >= $refreshToken->expiresAt()) {
                throw new InvalidGrantException('"refresh_token" expired');
            }

            // make sure the token is bound to the client
            if ($clientInfo->clientId() !== $refreshToken->clientId()) {
                throw new InvalidRequestException('unexpected "client_id"');
            }

            // scope in POST body MUST match scope stored in the refresh_token when
            // provided
            if (null !== $postScope = $request->post()->scope()) {
                if (!$postScope->isEqual($refreshToken->scope())) {
                    throw new InvalidRequestException('unexpected "scope"');
                }
            }

            // make sure the authorization still exists
            if (null === $authorizationInfo = $this->storage->getAuthorization($refreshToken->authKey())) {
                throw new InvalidGrantException('"refresh_token" is no longer authorized');
            }

            // check for refresh_token replay
            if ($this->storage->isRefreshTokenReplay($refreshToken->tokenId())) {
                // it was used before, delete the complete authorization
                $this->storage->deleteAuthorization($refreshToken->authKey());

                throw new InvalidGrantException('"refresh_token" was used before');
            }

            // log the use of the refresh_token so it can't be used again
            $this->storage->logRefreshToken($refreshToken->authKey(), $refreshToken->tokenId());

            // make sure an access_token never outlives its authorization, i.e.
            // the refresh_token expiry
            $accessTokenExpiresAt = $this->dateTime->add($this->accessTokenExpiry);
            if ($accessTokenExpiresAt > $authorizationInfo->expiresAt()) {
                $accessTokenExpiresAt = $authorizationInfo->expiresAt();
            }

            $accessToken = new AccessToken(
                Base64UrlSafe::encodeUnpadded($this->randomBytes()),
                $refreshToken->authKey(),
                $refreshToken->userInfo(),
                $refreshToken->clientId(),
                $refreshToken->scope(),
                $accessTokenExpiresAt
            );

            $refreshToken = new RefreshToken(
                Base64UrlSafe::encodeUnpadded($this->randomBytes()),
                $refreshToken->authKey(),
                $refreshToken->userInfo(),
                $refreshToken->clientId(),
                $refreshToken->scope(),
                $authorizationInfo->expiresAt()
            );

            // The (successful) use of a "refresh_token" indicates that the
            // "authorization" is still active. It is much easier to store the
            // moment at which a refresh_token for a particular "authorization"
            // was used than every individual use of an "access_token"...
            $this->storage->updateAuthorization($refreshToken->authKey(), $this->dateTime);

            return new JsonResponse(
                [
                    'access_token' => $this->tokenEncoder->encode($accessToken->json()),
                    'refresh_token' => $this->tokenEncoder->encode($refreshToken->json()),
                    'token_type' => 'bearer',
                    'expires_in' => $this->expiresAtToInt($accessTokenExpiresAt),
                ],
                // The authorization server MUST include the HTTP "Cache-Control"
                // response header field [RFC2616] with a value of "no-store" in any
                // response containing tokens, credentials, or other sensitive
                // information, as well as the "Pragma" response header field [RFC2616]
                // with a value of "no-cache".
                [
                    'Cache-Control' => 'no-store',
                    'Pragma' => 'no-cache',
                ]
            );
        } catch (TokenEncoderException $e) {
            throw new InvalidGrantException(\sprintf('invalid "refresh_token": %s', $e->getMessage()));
        }
    }

    /**
     * Identify and optionally authenticate the OAuth Client.
     *
     * Confidential clients require authentication using the
     * "client_secret_post" mechanism. This is required as per OAuth 2.1, we do
     * NO longer support "Basic Authentication", the mechanism required in
     * OAuth 2.0.
     */
    private function identifyAndAuthenticateClient(string $pClientId, ?string $pClientSecret): ClientInfo
    {
        $clientInfo = $this->getClient($pClientId);
        if (null !== $clientSecret = $clientInfo->clientSecret()) {
            // confidential client
            if (null === $pClientSecret) {
                throw new InvalidClientException('no "client_secret" provided');
            }
            if (false === hash_equals($clientSecret, $pClientSecret)) {
                throw new InvalidClientException('invalid "client_secret" provided');
            }
        }

        return $clientInfo;
    }

    /**
     * @see https://tools.ietf.org/html/rfc7636#appendix-A
     */
    private static function verifyPkce(string $codeVerifier, string $codeChallenge): bool
    {
        return hash_equals(
            $codeChallenge,
            Base64UrlSafe::encodeUnpadded(
                hash(
                    'sha256',
                    $codeVerifier,
                    true
                )
            )
        );
    }

    private function getClient(string $clientId): ClientInfo
    {
        if (null === $clientInfo = $this->clientDb->get($clientId)) {
            throw new InvalidClientException('client does not exist with this "client_id"');
        }

        return $clientInfo;
    }

    private function expiresAtToInt(DateTimeImmutable $expiresAt): int
    {
        return (int) ($expiresAt->getTimestamp() - $this->dateTime->getTimestamp());
    }

    /**
     * @param array<string,string> $queryParameters
     */
    private function prepareRedirectUri(string $redirectUri, array $queryParameters): string
    {
        return \sprintf(
            '%s%s%s',
            $redirectUri,
            false === strpos($redirectUri, '?') ? '?' : '&',
            http_build_query($queryParameters)
        );
    }
}
