<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server;

use RangeException;

/**
 * Extract data from an array, e.g. a JSON decoded string and enforce the type.
 */
class Extractor
{
    public static function requireArray(array $d, string $k): array
    {
        if (!\array_key_exists($k, $d)) {
            throw new RangeException(\sprintf('missing key "%s"', $k));
        }
        if (!\is_array($d[$k])) {
            throw new RangeException(\sprintf('key "%s" not of type `array`', $k));
        }

        return $d[$k];
    }

    public static function requireString(array $d, string $k): string
    {
        if (!\array_key_exists($k, $d)) {
            throw new RangeException(\sprintf('missing key "%s"', $k));
        }
        if (!\is_string($d[$k])) {
            throw new RangeException(\sprintf('key "%s" not of type `string`', $k));
        }

        return $d[$k];
    }

    public static function optionalString(array $d, string $k): ?string
    {
        if (!\array_key_exists($k, $d)) {
            return null;
        }

        return self::requireString($d, $k);
    }

    /**
     * @return ?array<string>
     */
    public static function optionalStringArray(array $d, string $k): ?array
    {
        if (!\array_key_exists($k, $d)) {
            return null;
        }

        return self::requireStringArray($d, $k);
    }

    /**
     * @return array<string>
     */
    public static function requireStringArray(array $d, string $k): array
    {
        if (!\array_key_exists($k, $d)) {
            throw new RangeException(\sprintf('missing key "%s"', $k));
        }
        // do we really need to copy to new array to satisfy static code
        // analysis? Seems wasteful...
        $resultSet = [];
        foreach ($d[$k] as $v) {
            if (!\is_string($v)) {
                throw new RangeException(\sprintf('key "%s" not of type `array<string>`', $k));
            }
            $resultSet[] = $v;
        }

        return $resultSet;
    }

    public static function requireBool(array $d, string $k): bool
    {
        if (!\array_key_exists($k, $d)) {
            throw new RangeException(\sprintf('missing key "%s"', $k));
        }
        if (!\is_bool($d[$k])) {
            throw new RangeException(\sprintf('key "%s" not of type `bool`', $k));
        }

        return $d[$k];
    }

    public static function optionalBool(array $d, string $k): ?bool
    {
        if (!\array_key_exists($k, $d)) {
            return null;
        }

        return self::requireBool($d, $k);
    }
}
