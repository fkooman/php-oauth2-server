HTTP_PORT ?= 8080

.PHONY: all test fmt psalm phpstan sloc dev

all:	test fmt psalm phpstan

test:
	# try system wide installations of PHPUnit first
	/usr/bin/phpunit || /usr/bin/phpunit9 || phpunit

fmt:
	php-cs-fixer fix

psalm:
	psalm

phpstan:
	phpstan

sloc:
	tokei src || cloc src

dev:
	@php -S localhost:$(HTTP_PORT) -t example example/router.php
