<?php

declare(strict_types=1);

$config = new PhpCsFixer\Config();

return $config->setRules(
    [
        '@PER' => true,
        '@PER:risky' => true,
        '@PHP74Migration' => true,
        '@PHP74Migration:risky' => true,
        '@PHPUnit84Migration:risky' => true,

        // Adds a default ``@coversNothing`` annotation to PHPUnit test classes
        // that have no ``@covers*`` annotation.
        'php_unit_test_class_requires_covers' => true,
        // Unused use statements must be removed.
        'no_unused_imports' => true,
        // Ordering use statements.
        'ordered_imports' => true,
        // Orders the elements of classes/interfaces/traits/enums.
        'ordered_class_elements' => true,
        // Ensures a single space after language constructs.
        'single_space_around_construct' => true,
        // An empty line feed must precede any configured statement.
        'blank_line_before_statement' => true,
        // Functions should be used with $strict param set to true.
        'strict_param' => true,
        // Annotations in PHPDoc should be grouped together so that annotations
        // of the same type immediately follow each other. Annotations of a
        // different type are separated by a single blank line.
        'phpdoc_separation' => true,
        // Add leading \ before constant invocation of internal constant to speed up resolving.
        'native_constant_invocation' => true,
        // Add leading \ before function invocation to speed up resolving.
        'native_function_invocation' => true,

        'header_comment' => [
            'header' => <<< 'EOD'
                Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>

                Permission is hereby granted, free of charge, to any person obtaining a copy
                of this software and associated documentation files (the "Software"), to deal
                in the Software without restriction, including without limitation the rights
                to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
                copies of the Software, and to permit persons to whom the Software is
                furnished to do so, subject to the following conditions:

                The above copyright notice and this permission notice shall be included in all
                copies or substantial portions of the Software.

                THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
                IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
                FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
                AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
                LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
                OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
                SOFTWARE.
                EOD,
        ],
    ]
)
    ->setRiskyAllowed(true)
    ->setFinder(PhpCsFixer\Finder::create()->in(__DIR__))
;
