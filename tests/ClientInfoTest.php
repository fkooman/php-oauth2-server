<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Tests;

use fkooman\OAuth\Server\ClientInfo;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class ClientInfoTest extends TestCase
{
    public function testNativeClientRedirectUri(): void
    {
        $clientInfo = ClientInfo::fromData(
            [
                'client_id' => 'org.eduvpn.app.windows',
                'redirect_uris' => ['http://127.0.0.1:{PORT}/callback', 'http://[::1]:{PORT}/callback'],
                'client_name' => 'eduVPN for Windows',
                'scope' => 'config',
            ]
        );

        $this->assertTrue($clientInfo->isValidRedirectUri('http://127.0.0.1:1234/callback'));
        $this->assertTrue($clientInfo->isValidRedirectUri('http://[::1]:1234/callback'));

        $this->assertFalse($clientInfo->isValidRedirectUri('http://127.0.0.1:1234/cb'));
        $this->assertFalse($clientInfo->isValidRedirectUri('http://127.0.0.1:0/callback'));
        $this->assertFalse($clientInfo->isValidRedirectUri('http://127.0.0.1:-5/callback'));

        // we try the literal "{PORT}", which should fail...
        // discovered during security audit of php-oauth2-server, but not a
        // security vulnerability as browsers do not know what to do with this
        // literal URL after it is accepted by the OAuth server
        // @see https://todo.sr.ht/~fkooman/php-oauth2-server/3
        $this->assertFalse($clientInfo->isValidRedirectUri('http://127.0.0.1:{PORT}/callback'));
        $this->assertFalse($clientInfo->isValidRedirectUri('http://[::1]:{PORT}/callback'));
    }

    public function testSimpleFromData(): void
    {
        $clientInfo = ClientInfo::fromData(
            [
                'client_id' => 'foobar',
                'redirect_uris' => ['https://client.example.org/callback'],
                'scope' => 'foo bar',
            ]
        );
        $this->assertSame('foobar', $clientInfo->clientId());
        $this->assertSame(['https://client.example.org/callback'], $clientInfo->redirectUris());
        $this->assertNull($clientInfo->clientSecret());
        $this->assertSame('foobar', $clientInfo->clientName());
        $this->assertTrue($clientInfo->requireApproval());
        $this->assertTrue($clientInfo->requirePkce());
        $this->assertSame(['foo', 'bar'], $clientInfo->allowedScope()->scopeTokenList());
    }

    public function testFullFromData(): void
    {
        $clientInfo = ClientInfo::fromData(
            [
                'client_id' => 'foobar',
                'redirect_uris' => ['https://client.example.org/callback'],
                'client_name' => 'Foo Bar',
                'client_secret' => 'foosecret',
                'require_approval' => false,
                'require_pkce' => false,
                'scope' => 'foo bar',
            ]
        );
        $this->assertSame('foobar', $clientInfo->clientId());
        $this->assertSame(['https://client.example.org/callback'], $clientInfo->redirectUris());
        $this->assertSame('foosecret', $clientInfo->clientSecret());
        $this->assertSame('Foo Bar', $clientInfo->clientName());
        $this->assertFalse($clientInfo->requireApproval());
        $this->assertFalse($clientInfo->requirePkce());
        $this->assertSame(['foo', 'bar'], $clientInfo->allowedScope()->scopeTokenList());
    }

    public function testSave(): void
    {
        $clientInfo = ClientInfo::fromData(
            [
                'client_id' => 'org.eduvpn.app.windows',
                'redirect_uris' => ['http://127.0.0.1:{PORT}/callback', 'http://[::1]:{PORT}/callback'],
                'client_name' => 'eduVPN for Windows',
                'scope' => 'config',
            ]
        );

        $this->assertSame(
            '{"client_id":"org.eduvpn.app.windows","redirect_uris":["http:\/\/127.0.0.1:{PORT}\/callback","http:\/\/[::1]:{PORT}\/callback"],"scope":"config","require_approval":true,"require_pkce":true,"require_nonce":true,"client_name":"eduVPN for Windows"}',
            $clientInfo->save()
        );
    }

    public function testLoadSave(): void
    {
        $clientInfoJson = '{"client_id":"org.eduvpn.app.windows","redirect_uris":["http:\/\/127.0.0.1:{PORT}\/callback","http:\/\/[::1]:{PORT}\/callback"],"scope":"config","require_approval":true,"require_pkce":true,"require_nonce":true,"client_name":"eduVPN for Windows"}';
        $clientInfo = ClientInfo::load($clientInfoJson);
        $this->assertSame($clientInfoJson, $clientInfo->save());
    }
}
