<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Tests;

use fkooman\OAuth\Server\Exception\InsufficientScopeException;
use fkooman\OAuth\Server\Exception\InvalidScopeException;
use fkooman\OAuth\Server\Scope;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ScopeTest extends TestCase
{
    public function testRequireAll(): void
    {
        $this->expectException(InsufficientScopeException::class);
        $this->expectExceptionMessage('insufficient_scope');
        $s = new Scope('foo bar');
        $s->requireAll(['foo', 'baz']);
    }

    public function testRequireAny(): void
    {
        $this->expectException(InsufficientScopeException::class);
        $this->expectExceptionMessage('insufficient_scope');
        $s = new Scope('foo bar');
        $s->requireAny(['baz']);
    }

    public function testRequireHas(): void
    {
        $s = new Scope('foo bar');
        $this->assertTrue($s->hasOne('bar'));
        $this->assertTrue($s->hasOne('foo'));
        $this->assertFalse($s->hasOne('foo bar'));
        $this->assertFalse($s->hasOne('baz'));
    }

    public function testIsEqual(): void
    {
        $ourScope = new Scope('foo');
        $this->assertTrue($ourScope->isEqual(new Scope('foo')));
        $this->assertFalse($ourScope->isEqual(new Scope('bar')));

        $ourScope = new Scope('foo bar');
        $this->assertTrue($ourScope->isEqual(new Scope('foo bar')));
        $this->assertTrue($ourScope->isEqual(new Scope('bar foo')));
        $this->assertFalse($ourScope->isEqual(new Scope('bar')));
        $this->assertFalse($ourScope->isEqual(new Scope('foo bar baz')));
    }

    public function testEmptyScope(): void
    {
        $this->expectException(InvalidScopeException::class);
        $this->expectExceptionMessage('invalid_scope');
        $ourScope = new Scope(''); // empty string is NOT allowed
    }

    public function testInvalidScopeSpace(): void
    {
        $this->expectException(InvalidScopeException::class);
        $this->expectExceptionMessage('invalid_scope');
        $ourScope = new Scope(' '); // space is NOT allowed
    }

    public function testInvalidScopeChar(): void
    {
        $this->expectException(InvalidScopeException::class);
        $this->expectExceptionMessage('invalid_scope');
        $ourScope = new Scope('🤣️'); // emoji is NOT allowed, invalid char
    }

    public function testContainsAny(): void
    {
        $scope = new Scope('foo');
        $this->assertTrue($scope->containsAny(new Scope('foo')));
        $this->assertTrue($scope->containsAny(new Scope('foo bar')));
        $this->assertTrue($scope->containsAny(new Scope('bar foo')));

        $this->assertFalse($scope->containsAny(new Scope('bar')));
        $this->assertFalse($scope->containsAny(new Scope('bar baz')));
    }

    public function testContainsAll(): void
    {
        $scope = new Scope('foo bar');
        $this->assertTrue($scope->containsAll(new Scope('foo bar')));
        $this->assertTrue($scope->containsAll(new Scope('bar foo')));
        $this->assertTrue($scope->containsAll(new Scope('foo')));
        $this->assertTrue($scope->containsAll(new Scope('bar')));

        $this->assertFalse($scope->containsAll(new Scope('foo bar baz')));
    }
}
