<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Tests;

use fkooman\OAuth\Server\Exception\TokenEncoderException;
use fkooman\OAuth\Server\Json;
use fkooman\OAuth\Server\SimpleEncryptedTokenEncoder;
use PHPUnit\Framework\TestCase;

/**
 * @covers \fkooman\OAuth\Server\SimpleEncryptedTokenEncoder
 *
 * @uses \fkooman\OAuth\Server\Base64UrlSafe
 * @uses \fkooman\OAuth\Server\Json
 */
final class SimpleEncryptedTokenEncoderTest extends TestCase
{
    private const SECRET_KEY = '{"alg":"XC20P","k":"9dC8BKSd1pPHDcKC1lX95j--oniuAA1USdWfiDBcOXo","kid":"zOkqj7LGIbrEoPo1gWapsA","kty":"oct","use":"enc"}';

    public function testSignVerify(): void
    {
        $tokenEncoder = new SimpleEncryptedTokenEncoder(SimpleEncryptedTokenEncoder::generateKey());
        static::assertSame('test', $tokenEncoder->decode($tokenEncoder->encode('test')));
    }

    public function testEncrypt(): void
    {
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder(self::SECRET_KEY, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::assertSame(
            'eyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6InpPa3FqN0xHSWJyRW9QbzFnV2Fwc0EifQ..AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB.aGrjSOhDPXFMjmWY7A.-1z2PR9bSeN4bB9N6ukGcw',
            $tokenEncoder->encode(Json::encode(['foo' => 'bar']))
        );
    }

    public function testEncryptEmpty(): void
    {
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder(self::SECRET_KEY, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::assertSame(
            'eyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6InpPa3FqN0xHSWJyRW9QbzFnV2Fwc0EifQ..AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB..54TEY9wMyY-yt-pHfi7czw',
            $tokenEncoder->encode('')
        );
    }

    public function testDecrypt(): void
    {
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder(self::SECRET_KEY, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::assertSame(
            Json::encode(['foo' => 'bar']),
            $tokenEncoder->decode('eyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6InpPa3FqN0xHSWJyRW9QbzFnV2Fwc0EifQ..AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB.aGrjSOhDPXFMjmWY7A.-1z2PR9bSeN4bB9N6ukGcw')
        );
    }

    public function testDecryptWithWrongKey(): void
    {
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder(self::SECRET_KEY, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::expectException(TokenEncoderException::class);
        // the `kid` does not match, so we fail on header check
        static::expectExceptionMessage('invalid token header');
        $tokenEncoder->decode('eyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6ImFGaXhLRlMxd1R3VkN6Zmt2RWU3Z0EifQ..eCYnUUqa0BBZ77iEWp277O23gVN4u1bI.EEbTLg.MAOi3zaoelajPLzWhS3-JQ');
    }

    public function testDecryptWithWrongKeyBytes(): void
    {
        // key has same `kid` as "real" key, but different `k`
        $fakeKeyId = '{"alg":"XC20P","k":"6p_gETkzUoR74IOV28HfXMbTBaDixvqVSBSSPPN1svU","kid":"zOkqj7LGIbrEoPo1gWapsA","kty":"oct","use":"enc"}';
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder($fakeKeyId, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::expectException(TokenEncoderException::class);
        static::expectExceptionMessage('unable to decrypt token');
        $tokenEncoder->decode('eyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6InpPa3FqN0xHSWJyRW9QbzFnV2Fwc0EifQ..AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB.aGrjSOhDPXFMjmWY7A.-1z2PR9bSeN4bB9N6ukGcw');
    }

    public function testMalformedToken(): void
    {
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder(self::SECRET_KEY, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::expectException(TokenEncoderException::class);
        static::expectExceptionMessage('invalid token header');
        $tokenEncoder->decode('XyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6InpPa3FqN0xHSWJyRW9QbzFnV2Fwc0EifQ..AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB.aGrjSOhDPXFMjmWY7A.-1z2PR9bSeN4bB9N6ukGcw');
    }

    public function testDecryptEmpty(): void
    {
        $tokenEncoder = new TestSimpleEncryptedTokenEncoder(self::SECRET_KEY, str_repeat("\x01", \SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
        static::assertSame(
            '',
            $tokenEncoder->decode('eyJhbGciOiJkaXIiLCJlbmMiOiJYQzIwUCIsImtpZCI6InpPa3FqN0xHSWJyRW9QbzFnV2Fwc0EifQ..AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB..54TEY9wMyY-yt-pHfi7czw')
        );
    }
}
