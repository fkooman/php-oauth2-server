<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Tests;

use DateTimeImmutable;
use Exception;
use fkooman\OAuth\Server\Base64UrlSafe;
use fkooman\OAuth\Server\ClientDbInterface;
use fkooman\OAuth\Server\ClientInfo;
use fkooman\OAuth\Server\Json;
use fkooman\OAuth\Server\OpenIDConfig;
use fkooman\OAuth\Server\OpenIDServer;
use fkooman\OAuth\Server\StorageInterface;

class TestOpenIDServer extends OpenIDServer
{
    private int $randomCounter = 1;

    public function __construct(StorageInterface $storage, ClientDbInterface $clientDb, DateTimeImmutable $dateTime)
    {
        parent::__construct(
            new OpenIDConfig(
                issuerIdentity: 'https://op.example.org',
                idTokenSigningAlgValuesSupported: ['none'],
                organizationName: 'Test Organization'
            ),
            $storage,
            $clientDb,
            new DummyTokenEncoder(),
            str_repeat("\x00", 32)
        );
        $this->dateTime = $dateTime;
    }

    /**
     * Get a random string of OAuthServer::RANDOM_LENGTH bytes.
     */
    protected function randomBytes(): string
    {
        return str_repeat((string) $this->randomCounter++, self::RANDOM_LENGTH);
    }

    protected function encodeIDToken(ClientInfo $clientInfo, array $tokenData): string
    {
        return \sprintf(
            '%s.%s',
            Base64UrlSafe::encodeUnpadded(Json::encode(['alg' => 'none'])),
            Base64UrlSafe::encodeUnpadded(Json::encode($tokenData))
        );
    }

    protected function decodeIDToken(string $encodedToken): array
    {
        $tokenHeader = Base64UrlSafe::encodeUnpadded(Json::encode(['alg' => 'none']));
        if (0 !== strpos($encodedToken, $tokenHeader . '.')) {
            throw new Exception('not a valid "id_token"');
        }
        $encodedTokenData = substr($encodedToken, \strlen($tokenHeader) + 1);
        $tokenData = Json::decode(Base64UrlSafe::decode($encodedTokenData));

        $unixTime = $this->dateTime->getTimestamp();
        if (!\array_key_exists('exp', $tokenData) || !\is_int($tokenData['exp'])) {
            throw new Exception('missing "exp" or not of type `int`');
        }
        if ($unixTime > $tokenData['exp']) {
            throw new Exception('token expired');
        }

        return $tokenData;
    }
}
