<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Server\Tests;

use DateInterval;
use DateTimeImmutable;
use fkooman\OAuth\Server\Base64UrlSafe;
use fkooman\OAuth\Server\ClientInfo;
use fkooman\OAuth\Server\Http\Request;
use fkooman\OAuth\Server\Json;
use fkooman\OAuth\Server\PdoStorage;
use fkooman\OAuth\Server\SimpleClientDb;
use fkooman\OAuth\Server\StorageInterface;
use PDO;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class OpenIDServerTest extends TestCase
{
    private StorageInterface $storage;
    private TestOpenIDServer $server;
    private DateTimeImmutable $dateTime;

    protected function setUp(): void
    {
        $this->dateTime = new DateTimeImmutable('2016-01-01T00:00:00+00:00');
        $clientDb = new SimpleClientDb();
        $clientDb->add(
            ClientInfo::fromData(
                [
                    'client_id' => 'openid-rp',
                    'scope' => 'openid',
                    'client_name' => 'OpenID Relying Party',
                    'redirect_uris' => ['https://example.org/callback'],
                    'sector_identifier' => 'example.org',
                ]
            )
        );
        $this->storage = new PdoStorage(new PDO('sqlite::memory:'));
        $this->storage->init();
        $this->server = new TestOpenIDServer(
            $this->storage,
            $clientDb,
            $this->dateTime
        );
    }

    public function testMetadata(): void
    {
        $this->assertSame(
            [
                'issuer' => 'https://op.example.org',
                'authorization_endpoint' => 'https://op.example.org/authorize',
                'token_endpoint' => 'https://op.example.org/token',
                'response_types_supported' => ['code'],
                'grant_types_supported' => ['authorization_code', 'refresh_token'],
                'token_endpoint_auth_methods_supported' => ['client_secret_post'],
                'code_challenge_methods_supported' => ['S256'],
                'scopes_supported' => ['openid'],
                'id_token_signing_alg_values_supported' => ['none'],
                'organization_name' => 'Test Organization',
                'userinfo_endpoint' => 'https://op.example.org/userinfo',
                'jwks_uri' => 'https://op.example.org/jwks',
                'end_session_endpoint' => 'https://op.example.org/end-session',
            ],
            $this->server->openIDMetadata(
                'https://op.example.org/authorize',
                'https://op.example.org/token',
                'https://op.example.org/userinfo',
                'https://op.example.org/jwks',
                'https://op.example.org/end-session'
            )
        );
    }

    public function testGetAuthorize(): void
    {
        static::assertSame(
            [
                'client_id' => 'openid-rp',
                'client_name' => 'OpenID Relying Party',
                'scope' => 'openid',
                'redirect_uri' => 'https://example.org/callback',
            ],
            $this->server->getAuthorize(
                new Request(
                    [],
                    [
                        'client_id' => 'openid-rp',
                        'redirect_uri' => 'https://example.org/callback',
                        'response_type' => 'code',
                        'scope' => 'openid',
                        'state' => '12345',
                        'nonce' => '54321',
                        'code_challenge_method' => 'S256',
                        'code_challenge' => 'E9Melhoa2OwvFrEMTJguCHaoeK1t8URWbuGJSstw-cM',
                    ],
                    []
                )
            )
        );
    }

    public function testEndSession(): void
    {
        $r = $this->server->endSession(
            'foo',
            new Request(
                [],
                [
                    'id_token_hint' => \sprintf(
                        '%s.%s',
                        Base64UrlSafe::encodeUnpadded(
                            Json::encode(
                                [
                                    'alg' => 'none',
                                ]
                            )
                        ),
                        Base64UrlSafe::encodeUnpadded(
                            Json::encode(
                                [
                                    'iss' => 'https://op.example.org',
                                    'aud' => 'openid-rp',
                                    'sub' => Base64UrlSafe::encodeUnpadded(
                                        hash(
                                            'sha256',
                                            \sprintf('%s%s%s', 'example.org', 'foo', str_repeat("\x00", 32)),
                                            true
                                        )
                                    ),
                                    'exp' => $this->dateTime->add(new DateInterval('PT5M'))->getTimestamp(),
                                    'nbf' => $this->dateTime->getTimestamp(),
                                ]
                            )
                        )
                    ),
                    'post_logout_redirect_uri' => 'https://example.org/',
                ],
                []
            )
        );
        $this->assertSame(302, $r->getStatusCode());
        $this->assertSame(
            [
                'Location' => 'https://example.org/',
                'Content-Type' => 'text/html; charset=utf-8',
            ],
            $r->getHeaders()
        );
    }
}
