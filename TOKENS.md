# Introduction

OAuth uses three types of "tokens", i.e. _authorization code_, 
_refresh token_ and _access token_. Each has their own characteristics. The 
_authorization code_ is used during _authorization_, i.e. the OAuth client will 
obtain it through a browser query parameter. This _authorization code_ is then
exchanged by the client for a _refresh token_ and _access token_.

| "Token"              | Single Use | Default Lifetime |
| -------------------- | ---------- | ---------------- |
| _authorization code_ | Yes        | 5 minutes        |
| _refresh_token_      | Yes        | 90 days          |
| _access_token_       | No         | 1 hour           | 

If ever an _authorization code_ or _refresh token_ is reused, the entire 
"authorization" must be revoked, i.e. after this they should all be invalidated
and the client needs to again obtain authorization.

Each of these tokens has some associated information, e.g. the client 
identifier that was used, the user that authorized the client, the "scope" that 
indicates which functionality the client was approved for by the user.

There's three approaches one can take dealing with tokens: 

1. use random strings as tokens and link them to the required information by
   using a database;
2. use digital signatures over the tokens where the tokens themselves contain 
   the information.
3. use a hybrid approach where some information is stored in the database, but 
   the remainder in the token itself.

Technically option 2 is not possible, as there needs to be a revocation 
mechanism and 'replay' needs to be detected as well. This leaves the other two
options.

Option 3 has some advantages:

1. Limit the amount of data stored in the database, not every _access token_ 
   needs an entry in the database;
2. By verifying signatures first, the database is not hit at all for invalid 
   tokens;
3. Allow "third party" _access token_ verification when revocation is not 
   necessary for a short lived _access token_ for the duration of 1 hour (when 
   using public key signatures).

For this OAuth server chose to implement a token signature mechanism that uses 
`HS256` (JSON Web Signatures) by default.
