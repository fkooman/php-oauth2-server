# ChangeLog

## 8.0.1 (2024-11-12)
- move to `codeberg.org`
- add Go implementation of the token format

## 8.0.0 (2024-05-31)
- keep track of when "authorizations" where last used 
  ([CB#1](https://codeberg.org/fkooman/php-oauth2-server/issues/1))
- "iss" is always required now 
  ([CB#2](https://codeberg.org/fkooman/php-oauth2-server/issues/2))

## 7.8.2 (2024-05-21)
- sort the results of `PdoStorage::getAuthorizations()` by the `authorized_at` 
  column in descending order

## 7.8.1 (2024-02-21)
- return "key id" in error messages if access token is not accepted because of
  wrong "key id"

## 7.8.0 (2023-08-24)
- remove workaround for eduVPN for Windows OAuth issue
  ([#8](https://todo.sr.ht/~fkooman/php-oauth2-server/8), 
   [#11](https://todo.sr.ht/~fkooman/php-oauth2-server/11), 
   [GitHub#218](https://github.com/Amebis/eduVPN/issues/218))
- simplify PKCE verification

## 7.7.0 (2023-07-05)
- implement "scope" registration for clients, allowing them only to request
  those scopes ([#13](https://todo.sr.ht/~fkooman/php-oauth2-server/13))
- add `Scope::isEqual` to compare scope objects
- add `Scope::containsAny` and `Scope::containsAll` that take scope objects 
  instead of `array` as parameter

## 7.6.1 (2023-04-24)
- implement URL decoding for username/password with Basic auth 
  ([#12](https://todo.sr.ht/~fkooman/php-oauth2-server/12))

## 7.6.0 (2023-04-12)
- allow per authorization expiry instead of always using the "global"
  `refreshTokenExpiry` ([#4](https://todo.sr.ht/~fkooman/php-oauth2-server/4))

## 7.5.3 (2023-03-02)
- fix issue with Windows workaround when dealing with invalid tokens
  ([#8](https://todo.sr.ht/~fkooman/php-oauth2-server/8))

## 7.5.2 (2023-02-09)
- make sure our workaround in 7.5.1 can't be misused to avoid token endpoint
  authentication with refresh tokens

## 7.5.1 (2023-02-09)
- handle issue with Windows eduVPN / Let's Connect! application 
  ([#218](https://github.com/Amebis/eduVPN/issues/218))

## 7.5.0 (2023-02-06)
- make sure the literal `{PORT}` can not be used by "native clients" 
  ([#3](https://todo.sr.ht/~fkooman/php-oauth2-server/3))
- implement `OAuthServer::metadata` 
  ([#2](https://todo.sr.ht/~fkooman/php-oauth2-server/2))
- improve and cleanup `example/`
  - always initialize the DB
  - expose OAuth server metadata
  - merge all endpoints in `index.php`
  - shared `MyClientDb` between `MyOAuthServer` and `MyBearerValidator`
  - prevent XSS in example
- implement `Scope::has()`
- use helper class `Extractor` to extract typed data from array
- expose `ClientInfo::redirectUriList`
- expose `ClientInfo::fromData` to load `ClientInfo` from array

## 7.4.0 (2023-01-03)
- allow overriding `Content-Type` response header with `JsonResponse`
- add `Scope::hasAny(array $requiredList): bool`
- small code formatting fixes
- rework client authentication
  - do not require `client_id` to be part of HTTP POST body in token request
    when Basic Authentication is used 
    ([#1](https://todo.sr.ht/~fkooman/php-oauth2-server/1)) 
  - perform client authentication *first* for token requests

## 7.3.1 (2022-11-07)
- fix `ini_get` on Debian 11 returning string value for 
  `mbstring.func_overload`

## 7.3.0 (2022-11-07)
- switch to `fkooman/put` for unit testing
- update `Makefile`
- drop `Binary` class
- make sure `mbstring.func_overload` is not enabled

## 7.2.0 (2022-10-14)
- add `iss` query parameter to OAuth callback (RFC 9207)
- fix token/key parsing issue with `explode()`

## 7.1.0 (2022-10-01)
- remove `ServerErrorException` and `UnsupportedResponseTypeException`
- remove `RandomInterface`, use "protected" `randomBytes` method in 
  `OAuthServer` that can be overridden for tests
- remove redundant boundary check for port range
- support multiple public keys for verifying signatures

## 7.0.0 (2022-01-04)
- major refactor touching almost all code
- require PHP >= 7.4
- drop requirement for polyfills
- switch to new [token format](TOKEN_FORMAT.md)
- reject refresh token re-use
- **NOTE**: breaks compatibility with 6.x tokens
- **NOTE**: database schema changed since 6.x

## 6.0.1 (2020-08-09)
- update README
- small test fixes
- no longer use `paragonie/constant_time_encoding` for `strlen` and `substr`
- require `ext-sqlite3`

## 6.0.0 (2020-01-20)
- remove recommendation for `league/oauth2-server`
- always enforce PKCE, also for confidential clients, see OAuth 2.1 
  [proposal](https://aaronparecki.com/2019/12/12/21/its-time-for-oauth-2-dot-1)

## 5.0.1 (2019-07-21)
- do not use the `error_description` field in the `WWW-Authenticate` response
  header when an invalid token was provided to avoid having to "escape" the 
  error message. The `error_description` is still available in the JSON body

## 5.0.0 (2019-03-27)
- implement token version check, reject tokens with wrong version
- remove `authz_time` from token again
- only record an authorization after the authorization code is used, simplifies
  database storage requirements
- update and simplify database schema
- remove refresh_token expiry

## 4.0.0 (2019-03-06)
- introduce simple `LocalSigner` which uses JWT tokens using HS256
- introduce `ClientDbInterface` and `ArrayClientDb` instead of a `callable`
- introduce `Scope` object in `AcessTokenInfo` instead of `string` scope value
- remove `SodiumSigner` and all dependencies on _sodium_
- drops compatibility with issued tokens, ALL tokens from `SodiumSigner` will
  become invalid!
- add `authz_time` to the tokens to record the time of the authorization by the 
  user
- make sure issued `access_token` will never outlive `refresh_token`

## 3.0.2 (2018-09-21)
- explicitly depend on versions of `paragonie/constant_time_encoding` that 
  support `Base64UrlSafe::encodeUnpadded`, drop hack
- simplify matching ports in redirect URI

## 3.0.1 (2018-06-08)
- use safe `strlen` and `substr` from `paragonie/constant_time_encoding`
- introduce `Util` class
- native function invocation (prefix all function calls with a `\`)
- support PHPUnit 7
- add `psalm.xml`
- relax `paragonie/random_compat` version requirement
- use `Base64UrlSafe::encodeUnpadded` if it is available

## 3.0.0 (2018-03-19)
- API changes
  - remove `OAuthServer::setExpiry`, `OAuthServer::setExpiresIn`
  - add `OAuthServer::setAccessTokenExpiry` and 
    `OAuthServer::setRefreshTokenExpiry`
  - `OAuthServer` constructor takes `SignerInterface` now instead of a string
    keypair, implementation available as `SodiumSigner`
  - add `SodiumSigner` which is compatible with previously issued access and 
    refresh tokens (from version ^2)
  - the `TokenInfo` object now has the `requireAnyScope` and `requireAllScope` 
    methods instead of `BearerValidator`
  - `SodiumSigner` takes the decoded keypair as parameter, no longer Base64 
    encoded
  - `SodiumSigner` takes decoded public keys as the second parameter to the 
    constructor, where the array key is the key ID
- introduce `RedirectResponse` for handling redirects;
- remove `HtmlResponse`
- change date format to `DateTime::ATOM` format in issued tokens
- no longer expire refresh tokens by default, requiring explicit call to
  `OAuthServer::setRefreshTokenExpiry` to make refresh tokens expire

## 2.2.0 (2018-01-10)
- all issued tokens are also "URL safe" now (without padding), Base64 encoded 
  tokens issued in previous versions are still valid
- introduce `OAuthServer::setExpiry` to allow specifying `DateInterval`
- deprecate `OAuthServer::setExpiresIn`
- allow specifying expiry for refresh tokens through `OAuthServer::setExpiry` 
  (issue #26)
  - new issued refresh tokens now **expire after 1 year** by default, old 
    issued refresh tokens will remain valid indefinitely or until user revokes
    the authorization

## 2.1.0 (2017-11-30)
- make it possible to disable requiring user approval for authorization of
  trusted clients

## 2.0.1 (2017-11-29)
- rework (lib)sodium compatibility
- encode the authorization code "URL safe" without padding
  - this fixes incompatibilities with Internet Explorer 11
  - this invalidates "in flight" authorization codes when a user is 
    authorizing at that particular moment. As codes are only valid for 5 
    minutes and the typical flow takes only a few seconds, this seems
    acceptable

## 2.0.0 (2017.11-14)
- remove "implicit grant" support, only support "authorization code"
- static code analysis fixes found by [Psalm](https://github.com/vimeo/psalm)
- rework Exception handling
- introduce Response objects
- support for PHPUnit 6
- do not accept tokens from deleted clients, API update (issue #14)
- `ClientInfo` now requires `redirect_uri_list`, MUST be `array`

## 1.1.0 (2017-09-18)
- introduce PHP >= 7.2 compatibility by using `SodiumCompat` wrapper;
- only use crypto functionality from Sodium, use polyfills for the rest;
- fix issues found by [Psalm](https://getpsalm.org/) and 
  [phpstan](https://github.com/phpstan/phpstan)

## 1.0.0 (2017-07-06)
- initial release
