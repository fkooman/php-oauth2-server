<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use fkooman\OAuth\Server\OAuthServer;
use fkooman\OAuth\Server\SimpleEncryptedTokenEncoder;
use fkooman\OAuth\Server\StorageInterface;

/**
 * "Wrapper" class to include our environment specifics.
 */
class MyOAuthServer extends OAuthServer
{
    public function __construct(string $issuerIdentity, StorageInterface $storage)
    {
        parent::__construct(
            $storage,
            new MyClientDb(),
            new SimpleEncryptedTokenEncoder('{"alg":"XC20P","k":"i4suLd8yUIxEvaSoD2gIG2VI4JVvZT1uFiTeLgxIQi0","kid":"Z0In7bRJQmffUQVSvj-Mtw","kty":"oct","use":"enc"}'),
            $issuerIdentity
        );

        // expire access_token after 30 seconds
        $this->accessTokenExpiry = new DateInterval('PT30S');
    }
}
