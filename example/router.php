<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * fix PATH_INFO for "/.well-known/..." for PHP's built in web server
 */
if (\array_key_exists('PATH_INFO', $_SERVER)) {
    include __DIR__ . '/index.php';

    return;
}

if (\array_key_exists('REQUEST_URI', $_SERVER)) {
    $pathInfo = $_SERVER['REQUEST_URI'];
    // remove the query string from the REQUEST_URI if it is there
    if (false !== $queryPosition = strpos($pathInfo, '?')) {
        $pathInfo = substr($pathInfo, 0, $queryPosition);
    }
    $_SERVER['PATH_INFO'] = $pathInfo;
}

include __DIR__ . '/index.php';
