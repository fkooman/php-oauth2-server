**Summary**: Simple and secure OAuth 2.0 server

**Description**: Opinionated, simple, easy to use OAuth 2.0 server for 
integrating OAuth 2.0 in your server applications using the latest (security) 
recommendations.

**License**: MIT

# Introduction

The project provides an opinionated OAuth 2.0 server library for integration in 
your own application. It has minimal dependencies, but still tries to be 
secure. The main purpose is to be as simple as possible whilst being secure.

This library supports all versions of PHP >= 7.4.

# Issue Tracker

The issue tracker can be found 
[here](https://codeberg.org/fkooman/php-oauth2-server/issues).

# Client Support

All (optional) OAuth authorization and token request parameters MUST always be
sent. PKCE is required for all client types.

# Features

- Supports PHP >= 7.4;
- Only supports _Authorization Code Grant_;
- Easy integration with your own application and/or framework;
- Does not force a framework on you;
- There are no toggles to shoot yourself in the foot with;
- Requires [PKCE](https://tools.ietf.org/html/rfc7636) for all client types;
- Supports single-use only refresh tokens;
- Does NOT implement RFC 6749 (#4.1.2.1) error responses (except for 
  `access_denied`);
- [OAuth 2.1](https://datatracker.ietf.org/doc/draft-ietf-oauth-v2-1/) draft 
  support 
- [Authorization Server Issuer Identification](https://tools.ietf.org/html/rfc9207)

# Requirements

Uses only core PHP extensions, no other dependencies.

# Use

Currently php-oauth2-server is not hosted on 
[Packagist](https://packagist.org/). It may be added in the future. In your 
`composer.json`:

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://codeberg.org/fkooman/php-oauth2-server"
        }
    ],
    "require": {
        "fkooman/oauth2-server": "^8"
    }
}
```

You can also download the signed source code archive from the project page
under "release notes".

# API

A simple, but complete example is included in the `example/` directory. The
`My*.php` files configure the OAuth server. You can run the example using the
included `Makefile`:

```bash
$ make dev
```

The OAuth server is configured on `http://localhost:8080/`. You can query the
_metadata_, e.g. to get information about the endpoint:

```bash
$ curl -s http://localhost:8080/.well-known/oauth-authorization-server | jq
{
    "authorization_endpoint": "http://localhost:8080/authorize",
    "code_challenge_methods_supported": [
        "S256"
    ],
    "grant_types_supported": [
        "authorization_code",
        "refresh_token"
    ],
    "issuer": "http://localhost:8080",
    "response_types_supported": [
        "code"
    ],
    "token_endpoint": "http://localhost:8080/token",
    "token_endpoint_auth_methods_supported": [
        "client_secret_post"
    ]
}
```

The example embeds a key. You MUST NOT use this key for your own applications. 
An example script is included to generate your own key:

```bash
$ php tools/generate_key.php
```

You can use 
[php-oauth2-client](https://codeberg.org/fkooman/php-oauth2-client) as a 
client to interact with this server, the example there is configured to work 
with this server out of the box.

# Contact

You can contact me with any questions or issues regarding this project. Drop
me a line at [fkooman@tuxed.net](mailto:fkooman@tuxed.net).

If you want to (responsibly) disclose a security issue you can also use the
PGP key with key ID `9C5EDD645A571EB2` and fingerprint
`6237 BAF1 418A 907D AA98  EAA7 9C5E DD64 5A57 1EB2`.

# License

[MIT](LICENSE).
